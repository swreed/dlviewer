# README #

Back in the early 1990s, before the internet had expanded beyond large universities, PC users traded files using dial-up modems and bulletin board systems (BBS).

At that time, a hodgepodge of different file formats existed for video animations: GIF, Grasp GL, Animator FLI, etc.

Among these was the DL animation file format, created by Italian programmers Luca de Gregorio and Davide Tom� for their company, DL-SOFT.  They also created and freely distributed DL-VIEW, a MS-DOS program that would display DL animation files.

The format became fairly popular, and DL animation files became commonly found on 1990s-era BBS systems (often, it must be said, containing adult content).

Time passed, technology marched on, and the DL format faded into obscurity, replaced by newer and more advanced multimedia standards. Meanwhile, on forgotten disks and subdirectories those old DL files still remained.

A while back I found some old DL files and started experimenting with how to display them. The result is this project - a simple DL file viewer, written in C# .NET.

### What is this repository for? ###

* DLViewer is a simple application that will display DL animation files, and also convert them to and from animated GIF files.
* Version 1.0

### How do I get set up? ###

* The application consists of a single executable file - DLViewer.exe.  A .zip file containing the compiled .exe file and a few sample DL files is in the repository; the file is named DLViewer_bin_1.0.0.zip
