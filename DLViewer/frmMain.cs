﻿
// ===================================================================
//
//  frmMain.cs - Main Windows Form implementation
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DLViewer
{
    /// <summary>
    /// frmMain - Main application form
    /// </summary>
    
    public partial class frmMain : Form
    {
        // Config settings
        private DLViewerSettings m_settings = new DLViewerSettings();

        // DL Animation reader
        private DLReader m_DL = new DLReader();
        private DLCommand m_currentDLCommand = null;

        // Timer used to display DL animation frames
        private Timer m_timer = new Timer();

        // Starting directory and filename, passed in from command-line
        private string m_initialDirectoryName = string.Empty;
        private string m_initialFileName = string.Empty;

        private string m_dlFilenameFilter = DLViewerSettings.DEFAULT_DIRECTORY_WILDCARD;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Args"></param>

        public frmMain(string[] Args)
        {
            ParseArgs(Args);

            InitializeComponent();
        }

        /// <summary>
        /// ParseArgs - Parse command-line arguments
        /// </summary>
        /// <param name="Args"></param>

        private void ParseArgs(string[] Args)
        {
            m_initialDirectoryName = string.Empty;
            m_initialFileName = string.Empty;

            // Were we given a filename or directory name on the command line?

            // Nope - just return
            if (Args == null || Args.Length < 1)
                return;

            // We were given a command-line argument
            // It could be a filename or a directory name

            string fileName = Args[0];

            if (File.Exists(fileName))
            {
                // Looks like its a filename
                m_initialFileName = fileName;

                // Set starting directory
                m_initialDirectoryName = Path.GetDirectoryName(fileName);
            }
            else if (Directory.Exists(fileName))
            {
                m_initialDirectoryName = fileName;
            }
        }

        /// <summary>
        /// frmMain_Load - Called on form Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Use double buffering to avoid screen flicker on repaint
            this.DoubleBuffered = true;

            // Load configuration settings
            LoadSettings();

            // Set filename wildcard used to load DL files from a directory
            this.m_dlFilenameFilter = string.IsNullOrEmpty(m_settings.FilenameWildcard) ? DLViewerSettings.DEFAULT_DIRECTORY_WILDCARD : m_settings.FilenameWildcard;
            
            // If we weren't given a filename or directory name as a command-line argument,
            // load the most recently used directory (if any)
            if (string.IsNullOrEmpty(m_initialDirectoryName) && string.IsNullOrEmpty(m_initialFileName))
            {
                m_initialDirectoryName = m_settings.PrevDirectoryName;
            }

            // Load DL file list with files in the directory
            if (!string.IsNullOrEmpty(m_initialDirectoryName))
            {
                this.txtDirectoryName.Text = m_initialDirectoryName;

                LoadFileList();
            }

            // If we have a starting filename, and it appears in
            // the list of DL filename we found in the directory, display it
            if (!string.IsNullOrEmpty(m_initialFileName))
            {
                int index = this.lbFiles.FindStringExact(Path.GetFileName(m_initialFileName));

                if (index >= 0 && index < this.lbFiles.Items.Count)
                    lbFiles.SelectedIndex = index;
            }

            SetupViewer();

            // Create a new Timer event handler 
            // We will display the next frame of the DL file on each timer tick
            m_timer.Tick += new EventHandler(this.OnTimer);

            StartTimer();

            // Set focus to the file list
            lbFiles.Select();
        }

        /// <summary>
        /// SetupViewer - set DL Viewer settings
        /// </summary>

        private void SetupViewer()
        {
            // SizeMode - either Actual Size or Zoomed
            this.pictureBox1.SizeMode = m_settings.ImageResizeMode;

            this.m_DL.RepeatAnimation = m_settings.RepeatAnimationFlag;
        }

        /// <summary>
        /// StartTimer
        /// </summary>

        private void StartTimer()
        {
            m_timer.Stop();
            m_timer.Interval = m_settings.FrameIntervalMilliseconds;
            m_timer.Start();
        }

        private void StopTimer()
        {
            m_timer.Stop();
        }

        /// <summary>
        /// OpenFile - Load the given DL File
        /// </summary>
        /// <param name="fileName"></param>

        private void OpenFile(string fileName)
        {
            if (!File.Exists(fileName) || this.m_DL.FileName == Path.GetFileName(fileName))
                return;

            string directoryName = Path.GetDirectoryName(fileName);

            this.txtDirectoryName.Text = directoryName;

            LoadFileList();

            int index = this.lbFiles.FindStringExact(Path.GetFileName(fileName));

            if (index >= 0 && index < this.lbFiles.Items.Count)
                lbFiles.SelectedIndex = index;
        }

        /// <summary>
        /// ClearImage
        /// </summary>

        private void ClearImage()
        {
            m_DL.Clear();

            txtProps.Text = string.Empty;

            this.saveToGIFToolStripMenuItem.Enabled = false;

            this.Invalidate();
        }

        /// <summary>
        /// OnTimer - Timer event handler
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>

        private void OnTimer(object obj, EventArgs args)
        {
            showNextImage();
        }

        private void showNextImage()
        {
            // If we haven't loaded a DL file, just display
            // a blank image.

            if (m_DL == null || m_DL.ImageCount < 1)
            {
                this.pictureBox1.Image = null;
                this.lblImage.Text = "No Image";
                this.pictureBox1.Image = null;
                return;
            }

            // We've load a DL file already...

            // Get next DL file command...
            RunCommand(m_DL.GetNextCommand());
        }

        private void RunCommand(DLCommand cmd)
        {
            // Skip over non-SHOW_IMAGE commands (eg, WAIT_KEY)...
            if (cmd == null || !cmd.IsImageType)
                return;

            m_currentDLCommand = cmd;

            // Display the next image from the DL file
            this.pictureBox1.Image = cmd.Image;

            // Display DL frame metadata 
            this.lblImage.Text = string.Format("Frame {0} of {1}, Image {2} of {3}", (cmd.FrameIndex + 1), m_DL.FrameCount, (cmd.ImageIndex + 1), m_DL.ImageCount);
        }

        /// <summary>
        /// lbFiles_SelectedIndexChanged
        /// User has selected a DL file from the file list to be displayed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void lbFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbFiles.Items.Count < 1)
                return;

            int selectedIndex = lbFiles.SelectedIndex;

            if (selectedIndex < 0 || selectedIndex >= lbFiles.Items.Count)
                return;

            FileListItem fi = (FileListItem)lbFiles.Items[selectedIndex];

            LoadFile(fi.FileName);
        }

        /// <summary>
        /// LoadFile - Display the given DL file
        /// </summary>
        /// <param name="fileName"></param>

        private void LoadFile(string fileName)
        {
            if (m_DL.FileName == Path.GetFileName(fileName))
                return;

            ClearImage();

            try
            {
                using (new WaitCursor())
                {
                    m_DL.Load(fileName);
                }

                string format = string.Empty;

                switch(m_DL.FileFormat)
                {
                    case DLConstants.DL_FORM_LARGE :
                        format = string.Format("{0}: Large ({1} x {2})", DLConstants.DL_FORM_LARGE, DLConstants.DL_LARGE_XSIZE, DLConstants.DL_LARGE_YSIZE);
                        break;
                    case DLConstants.DL_FORM_MEDIUM :
                        format = string.Format("{0}: Medium ({1} x {2})", DLConstants.DL_FORM_MEDIUM, DLConstants.DL_MEDIUM_XSIZE, DLConstants.DL_MEDIUM_YSIZE);
                        break;
                    case DLConstants.DL_FORM_SMALL :
                        format = string.Format("{0}: Small ({1} x {2})", DLConstants.DL_FORM_SMALL, DLConstants.DL_SMALL_XSIZE, DLConstants.DL_SMALL_YSIZE);
                        break;
                }

                string props = string.Format(
                    "FileName: {0}\r\nFileLength: {1} KB, {2} bytes\r\nTitle: {3}\r\nAuthor: {4}\r\nVersion: {5}\r\nFormat: {6}\r\nNumber of Screens: {7}\r\nNumber of Images: {8}\r\nImages Per Screen: {9}\r\nNumber of Frames: {10}\r\nNumber of Sounds: {11}\r\n",
                    m_DL.FileName,
                    (m_DL.FileLength / 1024), m_DL.FileLength,
                    m_DL.Title,
                    m_DL.AuthorName,
                    m_DL.FileVersion,
                    format,
                    m_DL.NumberOfScreens,
                    m_DL.NumberOfImages,
                    m_DL.ImagesPerScreen,
                    m_DL.NumberOfFrames,
                    m_DL.NumberOfSounds
                );

                props += string.Format
                (
                    "Number of Commands: {0}\r\nSHOW IMAGE Count: {1}\r\nBEGIN LOOP Count: {2}\r\nEND LOOP Count: {3}\r\nMax Loop Count: {4}\r\nWAIT KEY Count: {5}\r\nUnrecognized Command Count: {6}\r\n",
                    m_DL.CommandCount,
                    m_DL.ShowImageCmdCount,
                    m_DL.BeginLoopCmdCount,
                    m_DL.EndLoopCmdCount,
                    m_DL.MaxLoopCount,
                    m_DL.WaitKeyCmdCount,
                    m_DL.UnknownCmdCount
                );

                this.txtProps.Text = props;

                this.saveToGIFToolStripMenuItem.Enabled = true;

                m_settings.PrevDirectoryName = Path.GetDirectoryName(fileName);

                this.showNextImage();

                StartTimer();

                this.Invalidate();
            }
            catch (ArgumentException ex)
            {
                this.txtProps.Text = "Unable to load DL file: " + ex.Message;
            }
        }

        /// <summary>
        /// LoadFileList
        /// Fill DL file list with files from the selected directory
        /// </summary>

        private void LoadFileList()
        {
            // Reset active DL file, if any
            ClearImage();

            // Clear entries from file list
            this.lbFiles.Items.Clear();

            // Get current directory
            string directoryName = this.txtDirectoryName.Text;

            if (Directory.Exists(directoryName))
            {
                // Get list of DL animation files in the directory
                var fileList = Directory.EnumerateFiles(this.txtDirectoryName.Text, m_dlFilenameFilter);

                // Add them to the list control
                foreach (string fileName in fileList)
                {
                    lbFiles.Items.Add(new FileListItem(fileName));
                }
            }

            // Set file count
            lblFileCount.Text = (lbFiles.Items.Count != 1) ? string.Format("{0} Files", lbFiles.Items.Count) : "1 File";
        }

        /// <summary>
        /// menuMain_MenuActivate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void menuMain_MenuActivate(object sender, EventArgs e)
        {
            bool fileIsLoaded = ((m_DL != null) && (m_DL.NumberOfImages > 0));

            // Enable Save to GIF menu option only if we've loaded a DL file
            this.saveToGIFToolStripMenuItem.Enabled = fileIsLoaded;
        }

        /// <summary>
        /// Prompt user to select a DL file to view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.lbFiles.Items.Count > 0 && this.lbFiles.SelectedIndex >= 0)
            {
                FileListItem fi = (FileListItem)lbFiles.SelectedItem;

                openFileDialog1.InitialDirectory = Path.GetDirectoryName(fi.FileName);
            }

            openFileDialog1.Title = "Please select a DL Animation File";
            openFileDialog1.Filter = "DL Animation Files (*.dl)|*.dl|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.FileName = string.Empty;

            DialogResult result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                OpenFile(openFileDialog1.FileName);
            }
        }

        /// <summary>
        /// Prompt user to select a directory containing DL files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void openDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnBrowseDirectory_Click(sender, e);
        }

        /// <summary>
        /// Save frames from currently selected DL file to an animated GIF image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void saveToGIFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;

            if (this.lbFiles.Items.Count > 0 && this.lbFiles.SelectedIndex >= 0)
            {
                FileListItem fi = (FileListItem)lbFiles.Items[lbFiles.SelectedIndex];

                fileName = fi.ShortName;
            }

            if (string.IsNullOrEmpty(fileName))
                return;

            saveFileDialog1.Title = "Please enter name of target GIF Image File";
            saveFileDialog1.Filter = "GIF Image Files (*.gif)|*.gif|All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(fileName) + ".gif";

            DialogResult result = saveFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            fileName = saveFileDialog1.FileName;

            using (new WaitCursor())
            {
                m_DL.SaveFramesToGIF(fileName);
            }
        }

        /// <summary>
        /// Close the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Display About dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();

            about.ShowDialog(this);
        }

        /// <summary>
        /// Load config settings
        /// </summary>

        private void LoadSettings()
        {
            m_settings.Load();
        }

        /// <summary>
        /// Save config settings
        /// </summary>

        private void SaveSettings()
        {
            m_settings.Save();
        }

        /// <summary>
        /// Form Close event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveSettings();
        }

        /// <summary>
        /// Display Settings dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSettings frm = new frmSettings();

            if (frm.ShowDialog(this, m_settings) == System.Windows.Forms.DialogResult.OK)
            {
                SetupViewer();
                StartTimer();
            }
        }

        /// <summary>
        /// Click event handler for Browse Directory button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnBrowseDirectory_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtDirectoryName.Text))
            {
                this.folderBrowserDialog1.SelectedPath = this.txtDirectoryName.Text;
            }

            this.folderBrowserDialog1.ShowNewFolderButton = false;
            this.folderBrowserDialog1.Description = "Please select a Directory containing .DL files.";

            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.txtDirectoryName.Text = folderBrowserDialog1.SelectedPath;

                m_settings.PrevDirectoryName = folderBrowserDialog1.SelectedPath;

                LoadFileList();
            }
        }

        /// <summary>
        /// Convert animated GIF image to DL animation file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void convertAnimatedGIFToDLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Show the GIF-to-DL conversion dialog. 
            // If user creats a new DL animation file,
            // display it.

            frmConvert frm = new frmConvert();

            if (frm.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                this.OpenFile(frm.ConvertedFileName);
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            StopTimer();

            showNextImage();
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            StopTimer();

            m_DL.MoveToFirstCommand();

            showNextImage();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            StartTimer();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            StopTimer();

            m_DL.MoveToLastCommand();

            showNextImage();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            StopTimer();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && this.m_timer.Enabled == false)
            {
                Point screenPoint = ((Control)sender).PointToScreen(e.Location);
                Point formPoint = this.PointToClient(screenPoint);

                picContextMenu.Show(this, formPoint);
            }
        }

        private void mnuSavePicImage_Click(object sender, EventArgs e)
        {
            if (m_currentDLCommand == null || !m_currentDLCommand.IsImageType)
                return;

            saveFileDialog1.Title = "Please enter name of target Image File";
            saveFileDialog1.Filter = "PNG Image Files (*.png)|*.png|All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            string fileName = Path.GetFileNameWithoutExtension(m_DL.FileName) + "_" + m_currentDLCommand.ImageIndex + ".png";

            saveFileDialog1.FileName = fileName;

            DialogResult result = saveFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            fileName = saveFileDialog1.FileName;

            using (new WaitCursor())
            {
                m_currentDLCommand.Image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
            }
        }
    }

    // Internal class to store DL filename in list control
    internal class FileListItem
    {
        public FileListItem(string fileName)
        {
            this.FileName = fileName;
            this.ShortName = Path.GetFileName(fileName);
        }

        public override string ToString()
        {
            return this.ShortName;
        }

        public string FileName = string.Empty;
        public string ShortName = string.Empty;
    }
}
