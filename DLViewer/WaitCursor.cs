﻿
// ===================================================================
//
//  WaitCursor - Hourglass Cursor class
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
// ===================================================================

using System;
using System.Windows.Forms;

namespace DLViewer
{
    public class WaitCursor : IDisposable
    {
        //Private storage for replaced cursor. 
        //Assures that nested usage also works
        private Cursor m_prevCursor;

        // Default constructor : change mouse cursor to Cursors.WaitCursor
        public WaitCursor() : this(Cursors.WaitCursor) { }

        // Constructor: change mouse cursor to specific cursor
        public WaitCursor(Cursor c)
        {
            m_prevCursor = Cursor.Current;
            Cursor.Current = c;
        }

        // Destructor : change mouse pointer back to previous
        public void Dispose()
        {
            Cursor.Current = m_prevCursor;
        }
    }
}
