﻿
// ===================================================================
//
//  frmConvert.cs - GIF Conversion form implementation
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DLViewer
{
    // frmConvert - Dialog to convert animated GIF to a DL animation

    public partial class frmConvert : Form
    {
        public frmConvert()
        {
            InitializeComponent();

            // Name of newly created DL animation file
            this.ConvertedFileName = string.Empty;
        }

        public string ConvertedFileName { get; private set; }

        /// <summary>
        /// frmConvert_Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        private void frmConvert_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// cbBrowseSource_Click - Select GIF image to be converted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void cbBrowseSource_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Please select a GIF File";
            openFileDialog1.Filter = "GIF Images (*.gif)|*.gif|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            if (!string.IsNullOrEmpty(txtSourceFile.Text))
            {
                string directoryName = Path.GetDirectoryName(txtSourceFile.Text);

                openFileDialog1.InitialDirectory = directoryName;
            }

            DialogResult result = openFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            this.txtSourceFile.Text = openFileDialog1.FileName;

            EnableCreateButton();
        }

        /// <summary>
        /// cbBrowseTarget_Click - Select DL filename
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void cbBrowseTarget_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Title = "Please enter name of DL File to be created";
            saveFileDialog1.Filter = "DL Files (*.dl)|*.dl|All Files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 1;

            string fileName = txtTargetFile.Text;

            if (string.IsNullOrEmpty(fileName) && !string.IsNullOrEmpty(txtSourceFile.Text))
            {
                fileName = Path.Combine(Path.GetDirectoryName(txtSourceFile.Text), Path.GetFileNameWithoutExtension(txtSourceFile.Text) + ".dl");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                string directoryName = Path.GetDirectoryName(fileName);

                saveFileDialog1.InitialDirectory = directoryName;
                saveFileDialog1.FileName = Path.GetFileName(fileName);
            }

            DialogResult result = saveFileDialog1.ShowDialog();

            if (result != DialogResult.OK)
                return;

            this.txtTargetFile.Text = saveFileDialog1.FileName;

            EnableCreateButton();
        }

        /// <summary>
        /// EnableCreateButton
        /// </summary>

        private void EnableCreateButton()
        {
            /// Enable or Disable "Create DL" button
            /// depending on whether or not user has chosen
            /// a source GIF image file and target DL filename

            bool enable = (!string.IsNullOrEmpty(txtSourceFile.Text) && !string.IsNullOrEmpty(txtTargetFile.Text));

            cbCreate.Enabled = enable;
        }

        /// <summary>
        /// Create DL button click handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void cbCreate_Click(object sender, EventArgs e)
        {
            // Retrieve source and target filenames
            string sourceFile = txtSourceFile.Text;
            string targetFile = txtTargetFile.Text;

            // Set DL creation options:

            // DL File Format: Small, Medium or Large
            int fileFormat = DLConstants.DL_FORM_LARGE;

            if (this.rbSmall.Checked)
                fileFormat = DLConstants.DL_FORM_SMALL;
            else if (this.rbMedium.Checked)
                fileFormat = DLConstants.DL_FORM_MEDIUM;
            else
                fileFormat = DLConstants.DL_FORM_LARGE;

            DLWriterOptions opts = new DLWriterOptions();

            opts.FileFormat = fileFormat;

            // DL Title and Author Name
            opts.Title      = this.txtTitle.Text;
            opts.Author     = this.txtAuthor.Text;

            // Name of created DL animation file
            this.ConvertedFileName = string.Empty;

            // Read animated GIF image and create DL animation from it. 
            // Images from GIF file may be resized, depending on chosen DL file format

            try
            {
                // Perform conversion...

                using (new WaitCursor())
                {
                    DLWriter writer = new DLWriter();

                    writer.ConvertGIF(sourceFile, targetFile, opts);
                }

                // Success!
                // Return the name of the newly created DL animation file
                this.ConvertedFileName = targetFile;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (Exception ex)
            {
                // Uh Oh - there was a problem.
                // Display error message

                MessageBox.Show(ex.ToString(), "Unable to Convert GIF", MessageBoxButtons.OK, MessageBoxIcon.Error);

                this.ConvertedFileName = string.Empty;

                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            }

            this.Close();
        }

        /// <summary>
        /// cbCancel_Click - Cancel button click handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void cbCancel_Click(object sender, EventArgs e)
        {
            // Set blank result filename
            this.ConvertedFileName = string.Empty;

            // Set dialog result
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            // Quit
            this.Close();
        }
    }
}
