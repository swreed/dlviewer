﻿
// ===================================================================
//
//  DLWriter.cs - Convert Animated GIF Image to DL Animation File
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
//  Sample Usage:
//
//  // Set creation options
//  DLWriterOptions opts = new DLWriterOptions();
//  opts.FileFormat = DLConstants.DL_FORM_MEDIUM;
//  opts.Title      = "My Title";
//  opts.Author     = "John Doe";
//
//  // Source and target file
//  string sourceFile = @"c:\foo\image.gif";
//  string targetFile = @"c:\bar\anim.dl";
//
//  // Perform conversion
//  DLWriter writer = new DLWriter();
//  writer.ConvertGIF(sourceFile, targetFile, opts);
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using Gif.Components;
using System.IO;

namespace DLViewer
{
    /// <summary>
    /// DLWriterOptions - Container for DLWriter conversion options
    /// </summary>

    public class DLWriterOptions
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public DLWriterOptions() {}

        /// <summary>
        /// Constructor to specify a DL image format
        /// </summary>
        /// <param name="fileFormat">int - Either 1, 2 or 3</param>
        
        public DLWriterOptions(int fileFormat) : this(fileFormat, string.Empty, string.Empty) { }

        /// <summary>
        /// Contructor to specify a DL image format, title, and author value
        /// </summary>
        /// <param name="fileFormat">int - Either 1, 2 or 3</param>
        /// <param name="title">Title of DL Image - 20 chars max for DL version 2</param>
        /// <param name="author">Author's Name - 20 chars max for DL version 2</param>
        
        public DLWriterOptions(int fileFormat, string title, string author)
        {
            this.FileFormat = fileFormat;
            this.Title = title;
            this.Author = author;
        }

        /// <summary>
        /// FileFormat property - get/set DL File Format
        /// Value must be either 1, 2 or 3
        /// </summary>

        public int FileFormat
        {
            get { return m_fileFormat; }
            set 
            {
                switch (value)
                {
                    case DLConstants.DL_FORM_LARGE :
                    case DLConstants.DL_FORM_MEDIUM :
                    case DLConstants.DL_FORM_SMALL :
                        m_fileFormat = value;
                        break;

                    default :
                        throw new ArgumentException(string.Format("Unexpected DL Format Value {0} : Expected values {1}, {2} or {3}.", value, DLConstants.DL_FORM_LARGE, DLConstants.DL_FORM_MEDIUM, DLConstants.DL_FORM_SMALL));
                }
            }
        }

        /// <summary>
        /// DL File Title - 20 chars max for DL Version 2
        /// </summary>

        public string Title  { get { return m_title; } set { m_title = CheckTextValue(value, DLConstants.DL_TEXT_LENGTH_V2); } }

        /// <summary>
        /// DL Author Name - 20 chars max for DL version 2
        /// </summary>
        
        public string Author { get { return m_author; } set { m_author = CheckTextValue(value, DLConstants.DL_TEXT_LENGTH_V2); } }

        /// <summary>
        /// Set background color used by image
        /// </summary>
        
        public Color    BackgroundColor { get { return m_backgroundColor; } set { m_backgroundColor = value; } }

        private int     m_fileFormat = DLConstants.DL_FORM_LARGE;
        private string  m_title = string.Empty;
        private string  m_author = string.Empty;
        private Color   m_backgroundColor = Color.Black;

        /// <summary>
        /// CheckTextValue
        /// Check length of given text value against max length
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>

        private string CheckTextValue(string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            if (value.Length > maxLength)
                value = value.Substring(0, maxLength);

            return value;
        }
    }

    /// <summary>
    /// DLWriter - Converts an animated GIF image into a version 2 DL animation file
    /// </summary>

    public class DLWriter
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        
        public DLWriter() { }

        // Background color used to fill in margins when resizing images
        private Color m_marginColor = Color.White;

        // GIF reader class instance
        private GifDecoder m_gifReader;

        // Number of screens to be written from GIF to DL file
        private int m_screenCount = 0;
        private int m_screenWidth = DLConstants.DL_LARGE_XSIZE;
        private int m_screenHeight = DLConstants.DL_LARGE_YSIZE;

        private int m_fileVersion = DLConstants.DL_VERSION_2;
        private int m_fileFormat = DLConstants.DL_FORM_LARGE;

        // Number of images to be written from GIF to DL file
        private int m_imageCount = 0;

        private int m_imagesPerScreen = 0;
        private int m_imageWidth = 0;
        private int m_imageHeight = 0;

        // Length of text fields in the DL file: varies by version
        private int m_textFieldLength = DLConstants.DL_TEXT_LENGTH_V2;

        // 256-color palette copied from GIF image
        private Color[] m_palette;

        // Lookup table for color matching
        private Dictionary<Color, int> m_colorIndex = new Dictionary<Color, int>();

        /// <summary>
        /// ConvertGIF - Convert animated GIF image to DL animation file
        /// </summary>
        /// <param name="sourceFileName">Filename of source animated GIF image</param>
        /// <param name="targetFileName">Filename of DL Animation to be created</param>
        /// <param name="options">Conversion options</param>
        
        public void ConvertGIF(string sourceFileName, string targetFileName, DLWriterOptions options)
        {
            ReadGIFFile(sourceFileName);

            using (FileStream fs = new FileStream(targetFileName, FileMode.Create, FileAccess.Write))
            {
                using (BinaryWriter writer = new BinaryWriter(fs))
                {
                    WriteDLFile(writer, options);
                }
            }
        }

        /// <summary>
        /// ReadGIFFile - Load animated GIF image file that will
        /// be converted to a DL animation
        /// </summary>
        /// <param name="fileName">Filename of GIF image to be read</param>

        private void ReadGIFFile(string fileName)
        {
            if (!File.Exists(fileName))
                throw new FileNotFoundException(string.Format("GIF Image File {0} not found", fileName));
            
            m_gifReader = new GifDecoder();

            int errorCode = m_gifReader.Read(fileName);

            if (errorCode != GifDecoder.STATUS_OK)
            {
                string errorMessage = string.Empty;

                if (errorCode == GifDecoder.STATUS_OPEN_ERROR)
                {
                    errorMessage = "Unable to open source file";
                }
                else
                {
                    // GifDecoder.STATUS_FORMAT_ERROR or other error
                    errorMessage = "Error decoding file";
                }

                throw new ApplicationException(string.Format("Failed to read GIF image file {0} : {1} ({2})", fileName, errorMessage, errorCode));
            }
        }

        /// <summary>
        /// SetFormat
        /// Set various constants based on chosen DL file format
        /// </summary>
        /// <param name="fileFormat"></param>

        private void SetFormat(int fileFormat)
        {
            // Set image resolution depending on chosen format
            switch(fileFormat)
            {
                case DLConstants.DL_FORM_LARGE:
                {
                    m_imagesPerScreen = 1;
                    m_imageHeight     = DLConstants.DL_LARGE_YSIZE;
                    m_imageWidth      = DLConstants.DL_LARGE_XSIZE;
                    break;
                }

                case DLConstants.DL_FORM_MEDIUM:
                {
                    m_imagesPerScreen = 4;
                    m_imageHeight     = DLConstants.DL_MEDIUM_YSIZE;
                    m_imageWidth      = DLConstants.DL_MEDIUM_XSIZE;
                    break;
                }

                case DLConstants.DL_FORM_SMALL:
                {
                    m_imagesPerScreen   = 16;
                    m_imageHeight       = DLConstants.DL_SMALL_YSIZE;
                    m_imageWidth        = DLConstants.DL_SMALL_XSIZE;
                    break;
                }

                default:
                {
                    throw new ArgumentException(string.Format("Unexpected DL Format Value {0} : Expected values {1}, {2} or {3}.", fileFormat, DLConstants.DL_FORM_LARGE, DLConstants.DL_FORM_MEDIUM, DLConstants.DL_FORM_SMALL));
                }
            }

            // We support creation of version 2 DL files...
            m_fileFormat        = fileFormat;
            m_fileVersion       = DLConstants.DL_VERSION_2;
            m_textFieldLength   = DLConstants.DL_TEXT_LENGTH_V2;

            m_screenHeight = DLConstants.DL_LARGE_YSIZE;
            m_screenWidth  = DLConstants.DL_LARGE_XSIZE;

            // A DL file can contain a maximum of 256 screens.
            // If GIF file contains more than that, only write the first 256.
            m_imageCount = Math.Min(m_gifReader.GetFrameCount(), DLConstants.DL_MAX_SCREENS);

            // Set number of screens per image
            m_screenCount = m_imageCount / m_imagesPerScreen;

            if ((m_imageCount % m_imagesPerScreen) > 0)
                m_screenCount++;
        }

        /// <summary>
        /// WriteTextValue
        /// </summary>
        /// <param name="value"></param>
        /// <param name="writer"></param>

        private void WriteTextValue(string value, int maxLength, BinaryWriter writer)
        {
            int length = string.IsNullOrEmpty(value) ? 0 : Math.Min(value.Length, maxLength);

            // DL file text fields are stored in the file with each character XOR'd with 0xff
            for (int i = 0; i < length; i++)
            {
                byte b = (byte)((uint)value[i] ^ (uint)0xff);

                writer.Write(b);
            }

            // If text value is shorter than the max length,
            // pad the remaining field length with nulls
            for (int i = length; i < maxLength; i++)
                writer.Write((byte)0x00);
        }

        /// <summary>
        /// WriteDLFile
        /// Write output DL file using images loaded from input GIF file
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="fileFormat"></param>

        private void WriteDLFile(BinaryWriter writer, DLWriterOptions options)
        {
            SetFormat(options.FileFormat);

            // DL Version ID
            writer.Write((byte)m_fileVersion);

            // Image Format
            writer.Write((byte)m_fileFormat);

            // Write Title
            WriteTextValue(options.Title, m_textFieldLength, writer);

            // Write Author Name
            WriteTextValue(options.Author, m_textFieldLength, writer);

            // Number of screens
            writer.Write((byte)m_screenCount);

            // Number of images
            writer.Write((Int32)m_imageCount);

            // Write palette
            WriteDLPalette(writer);

            // Write screens
            WriteDLScreens(writer);

            // Write frames
            WriteDLFrames(writer);
        }

        /// <summary>
        /// Create a lookup table of the color values in the DL palette
        /// for faster color matching of GIF frames
        /// </summary>

        private void CreateColorIndex()
        {
            m_colorIndex.Clear();

            for (int i = 0; i < m_palette.Length; i++)
            {
                m_colorIndex[m_palette[i]] = i;
            }
        }

        /// <summary>
        /// FindClosestColor - Find of the closest match
        /// to the given color in the DL color palette.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="palette"></param>
        /// <returns></returns>

        private int FindClosestColor(Color c)
        {
            // If color is already in index, use it
            if (m_colorIndex.ContainsKey(c))
                return m_colorIndex[c];

            int r = c.R;
            int g = c.G;
            int b = c.B;
            int minpos = 0;
            int dmin = 256 * 256 * 256;
            
            int len = m_palette.Length;

            // TODO: Performance of this search could be
            // improved if we sorted the palette colors

            for (int i = 0; i < m_palette.Length; i++)
            {
                int dr = Math.Abs(r - (int)m_palette[i].R);
                int dg = Math.Abs(g - (int)m_palette[i].G);
                int db = Math.Abs(b - (int)m_palette[i].B);

                int d = dr * dr + dg * dg + db * db;

                // exact match
                if (d == 0)
                {
                    m_colorIndex[c] = i;
                    return i;
                }

                if (d < dmin)
                {
                    dmin = d;
                    minpos = i;
                }
            }

            // Save color in index
            m_colorIndex[c] = minpos;

            return minpos;
        }

        /// <summary>
        /// WriteVGAPalette
        /// </summary>
        /// <param name="writer"></param>

        private void WriteVGAPalette(BinaryWriter writer)
        {
            // Convert image palette to VGA format :
            // Each R/G/B color ranges from 0-63 instead of 0-255

            for (int i = 0; i < m_palette.Length; i++)
            {
                Color c = m_palette[i];

                byte red   = GetVGAColorValue(c.R);
                byte green = GetVGAColorValue(c.G);
                byte blue  = GetVGAColorValue(c.B);

                writer.Write(red);
                writer.Write(green);
                writer.Write(blue);
            }
        }

        /// <summary>
        /// Convert a R/G/B color value from 0-255 range to VGA 0-63 range.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>

        private byte GetVGAColorValue(byte color)
        {
            // Scale color value from (0-255) to (0-63)
            return (byte)Math.Min((int)Math.Round(color / 4.0), 0x3f);
        }

        /// <summary>
        /// WriteDLPalette
        /// </summary>
        /// <param name="writer"></param>

        private void WriteDLPalette(BinaryWriter writer)
        {
            // First, try to get the Global Palette from the GIF file...
            m_palette = m_gifReader.GetGlobalColorTable();

            if (m_palette == null)
            {
                // If there is no Global palette, use the local palette from the first frame
                m_palette = m_gifReader.GetGIFFrame(0).lct;
            }

            // If we can't find the palette, throw an error
            if (m_palette == null)
                throw new ApplicationException("Unable to convert GIF : Color Table not found");

            // We only support 256-color GIF images
            if (m_palette.Length != DLConstants.DL_MAX_COLORS)
                throw new ApplicationException(string.Format("Unable to convert GIF : Color Table has {0} colors, expected {1}", m_palette.Length, DLConstants.DL_MAX_COLORS));

            // Check if the palette contains a matching color for our current margin color
            SetMarginColor();

            // Index the palette colors for faster color matching when we write screens
            CreateColorIndex();

            // Scale palette to VGA color range and write it to the DL file
            WriteVGAPalette(writer);
        }

        /// <summary>
        /// WriteDLScreens
        /// </summary>
        /// <param name="writer"></param>

        private void WriteDLScreens(BinaryWriter writer)
        {
            int expectedPixelCount = (m_screenWidth * m_screenHeight);

            int xperscreen = m_screenWidth / m_imageWidth;

            for (int i = 0; i < m_screenCount; i++)
            {
                using (Bitmap screen = new Bitmap(m_screenWidth, m_screenHeight))
                {
                    using (System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(screen))
                    {
                        for (int j = 0; j < this.m_imagesPerScreen; j++)
                        {
                            int imageIndex = (i * m_imagesPerScreen) + j;

                            if (imageIndex >= this.m_imageCount)
                                continue;

                            GifDecoder.GifFrame frame = m_gifReader.GetGIFFrame(imageIndex);

                            using (Bitmap image = ResizeImage((Bitmap)frame.image, m_imageWidth, m_imageHeight))
                            {
                                int xoffset = (j % xperscreen) * m_imageWidth;
                                int yoffset = (j / xperscreen) * m_imageHeight;

                                graphic.DrawImage(image, xoffset, yoffset, m_imageWidth, m_imageHeight);
                            }
                        }
                    }

                    byte[] pixels = RemapScreenColors(screen);

                    if (pixels.Length != expectedPixelCount)
                        throw new ApplicationException(string.Format("Unable to convert GIF : Screen {0} pixel length is {1} - expected {2}", i, pixels.Length, expectedPixelCount));

                    // Write the bitmap screen to the DL file
                    writer.Write(pixels);
                }
            }
        }
        
        /// <summary>
        ///  SetMarginColor
        ///  Check color palette and see if it contains a matching color for our margin color.
        ///  If not, find a duplicate color entry in the palette and use it for the margin color.
        /// </summary>

        private void SetMarginColor()
        {
            // First, check aspect ratios
            double aspect1 = (double)m_imageWidth / (double)m_imageHeight;
            double aspect2 = (double)m_gifReader.GetFrameSize().Width / (double)m_gifReader.GetFrameSize().Height;

            // If aspect ratios match, there will be no margin in the resized image,
            // and therefore no need for margin color

            if (Math.Abs(aspect1 - aspect2) < 1e-3)
                return;

            // Count the number of unique colors in the palette
            SortedDictionary<int, int> colorCounts = new SortedDictionary<int, int>();

            int maxCount = -1;
            int maxIndex = 0;
            int count = 0;

            // Get color number of margin color
            int marginIndex = 0x10000 * m_marginColor.B + 0x100 * m_marginColor.G + m_marginColor.R;

            for (int i = 0; i < m_palette.Length; i++)
            {
                Color c = m_palette[i];

                // color number = 65536 * blue + 256 * green + red 
                int colorIndex = 0x10000 * c.B + 0x100 * c.G + c.R;

                // If the margin color is already in the palette - just return
                if (colorIndex == marginIndex)
                    return;

                if (!colorCounts.ContainsKey(colorIndex))
                {
                    count = 1;

                    colorCounts[colorIndex] = count;
                }
                else
                {
                    count = colorCounts[colorIndex];

                    count++;

                    colorCounts[colorIndex] = count;
                }

                if (count > maxCount)
                {
                    maxCount = count;
                    maxIndex = i;
                }
            }

            // If there are one or more duplicate colors in the palette
            // use one of the duplicate entries for the margin color.
            if (maxCount > 1)
                m_palette[maxIndex] = m_marginColor;
        }

        /// <summary>
        /// RemapScreenColors
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns></returns>

        private byte[] RemapScreenColors(Bitmap bitmap)
        {
            int bytes = bitmap.Width * bitmap.Height;

            byte[] pixels = new byte[bytes];

            // Iterate through each pixel of the bitmap and
            // find the closest match for its color 
            // in the DL file's 256-color palette

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    Color frameColor = bitmap.GetPixel(x, y);

                    int mappedColorIndex = this.FindClosestColor(frameColor);

                    int pixelIndex = (y * bitmap.Width) + x;

                    pixels[pixelIndex] = (byte)mappedColorIndex;
                }
            }

            // Return resized, recolored bitmap
            return pixels;
        }

        /// <summary>
        /// WriteDLFrames
        /// </summary>
        /// <param name="writer"></param>

        private void WriteDLFrames(BinaryWriter writer)
        {
            // Just write a DL SHOW_IMAGE command
            // for each screen...

            for (int screen = 0; screen < m_gifReader.GetFrameCount(); screen++)
            {
                writer.Write((Int16)screen);
            }
        }

        /// <summary>
        /// ResizeImage
        /// Resize the given image to the given size, preserving aspect ratio.
        /// If the image doesn't fit exactly within the given area, the margins
        /// are filled with the current background color.
        /// </summary>
        /// <param name="sourceImage"></param>
        /// <param name="canvasWidth"></param>
        /// <param name="canvasHeight"></param>
        /// <returns></returns>

        private Bitmap ResizeImage(Bitmap sourceImage, int canvasWidth, int canvasHeight)
        {
            int originalWidth = sourceImage.Width;
            int originalHeight = sourceImage.Height;

            Bitmap resizedImage = new Bitmap(canvasWidth, canvasHeight);

            using (System.Drawing.Graphics graphic = System.Drawing.Graphics.FromImage(resizedImage))
            {
                // Use high quality resizing
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphic.CompositingQuality = CompositingQuality.HighQuality;

                // Figure out the ratio
                double ratioX = (double)canvasWidth / (double)originalWidth;
                double ratioY = (double)canvasHeight / (double)originalHeight;

                // Use whichever multiplier is smaller
                double ratio = ratioX < ratioY ? ratioX : ratioY;

                // Now we can get the new height and width
                int newHeight = Convert.ToInt32(originalHeight * ratio);
                int newWidth = Convert.ToInt32(originalWidth * ratio);

                // Now calculate the X,Y position of the upper-left corner 
                // (one of these will always be zero)
                int posX = Convert.ToInt32((canvasWidth - (originalWidth * ratio)) / 2);
                int posY = Convert.ToInt32((canvasHeight - (originalHeight * ratio)) / 2);

                // Pad margins with margin color
                if (newHeight != canvasHeight || newWidth != canvasWidth)
                    graphic.Clear(m_marginColor);

                // Draw image with the new size
                graphic.DrawImage(sourceImage, posX, posY, newWidth, newHeight);
            }

            return resizedImage;
        }
    }
}
