﻿
namespace DLViewer
{
    /// <summary>
    /// DLConstants - Container for DL Animation constant values
    /// </summary>

    public static class DLConstants
    {
        // DL File Version numbers
        public const int DL_VERSION_1 = 1;
        public const int DL_VERSION_2 = 2;
        public const int DL_VERSION_3 = 3;

        // DL Image Formats
        public const int DL_FORM_LARGE = 0;
        public const int DL_FORM_MEDIUM = 1;
        public const int DL_FORM_SMALL = 2;

        // Image Format dimensions
        public const int DL_LARGE_XSIZE = 320;
        public const int DL_LARGE_YSIZE = 200;
        public const int DL_MEDIUM_XSIZE = 160;
        public const int DL_MEDIUM_YSIZE = 100;
        public const int DL_SMALL_XSIZE = 80;
        public const int DL_SMALL_YSIZE = 50;

        public const int DL_MAX_COLORS = 256;
        public const int DL_MAX_SCREENS = 256;

        // Length of Title & Author text fields
        public const int DL_TEXT_LENGTH_V1 = 20;
        public const int DL_TEXT_LENGTH_V2 = 20;
        public const int DL_TEXT_LENGTH_V3 = 40;

        // Frame command values
        public const uint DL_CMD_MAX_IMAGE      = 0x8000;
        public const uint DL_CMD_BEGIN_LOOP     = 0xffff;
        public const uint DL_CMD_END_LOOP       = 0xfffe;
        public const uint DL_CMD_WAIT_KEY       = 0xfffd;
        public const uint DL_CMD_CLS            = 0xfff4;  // Clear the screen
        public const uint DL_CMD_BEGIN_CYCLE    = 0xffff;
        public const uint DL_CMD_END_CYCLE      = 0xfffe;
    }
}
