﻿
// ===================================================================
//
//  frmSettings.cs - Settings Form implementation
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DLViewer
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();
        }

        private DLViewerSettings m_settings = null;

        public DialogResult ShowDialog(IWin32Window owner, DLViewerSettings settings)
        {
            m_settings = settings;

            DialogResult result = base.ShowDialog(owner);

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                LoadSettingsFromGUI();
            }

            return result;
        }

        private void LoadGUIFromSettings()
        {
            if (m_settings.ImageResizeMode == PictureBoxSizeMode.Zoom)
            {
                rbResize.Checked = true;
                rbActualSize.Checked = false;
            }
            else
            {
                rbResize.Checked = false;
                rbActualSize.Checked = true;
            }

            udFrameDelay.Value = m_settings.FrameIntervalMilliseconds;

            cbxRepeat.Checked = m_settings.RepeatAnimationFlag;
        }

        private void LoadSettingsFromGUI()
        {
            m_settings.ImageResizeMode = rbResize.Checked ? PictureBoxSizeMode.Zoom : PictureBoxSizeMode.CenterImage;

            m_settings.FrameIntervalMilliseconds = (int)udFrameDelay.Value;

            m_settings.RepeatAnimationFlag = cbxRepeat.Checked;
        }

        private void frmSettings_Load(object sender, EventArgs e)
        {
            LoadGUIFromSettings();
        }

        private void cbOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void cbCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void udFrameDelay_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
