﻿using System.Drawing;

namespace DLViewer
{
    /// <summary>
    /// DLCommand - Container for a command from one DL File "frame"
    /// </summary>

    public class DLCommand
    {
        public DLCommand(DLCommandType type)
        {
            this.m_cmdType = type;
        }

        public  DLCommandType   CommandType     { get { return m_cmdType;           } set { m_cmdType           = value; } }    // Type of DL command

        public  bool            IsImageType         { get { return (this.CommandType == DLCommandType.SHOW_IMAGE); } }
        public  bool            IsWaitKeyType       {  get { return (this.CommandType == DLCommandType.WAIT_KEY);  } }
        public  bool            IsBeginLoopType     { get { return (this.CommandType == DLCommandType.BEGIN_LOOP); } }
        public  bool            IsEndLoopType       { get { return (this.CommandType == DLCommandType.END_LOOP);   } }
        public bool             IsClearScreenType   {  get { return (this.CommandType == DLCommandType.CLEAR_SCREEN);  } }

        public  uint            CommandCode     { get { return m_cmdCode;           } set { m_cmdCode           = value; } }    // DL Command code
        public  Bitmap          Image           { get { return m_image;             } set { m_image             = value; } }    // Bitmap image - valid for SHOW_IMAGE commands
        public  int             ImageIndex      { get { return m_imageIndex;        } set { m_imageIndex        = value; } }    // Image index from DL file - valid for SHOW_IMAGE commands
        public  int             FrameIndex      { get { return m_frameIndex;        } set { m_frameIndex        = value; } }    // Frame index from DL file
        public  int             LoopCount       { get { return m_loopCount;         } set { m_loopCount         = value; } }    // Loop count - valid for BEGIN_LOOP commands - may have been validated
        public  int             OrigLoopCount   { get { return m_origLoopCount;     } set { m_origLoopCount     = value; } }    // Original Loop count from File - valid for BEGIN_LOOP commands
        public  int             BeginLoopIndex  { get { return m_beginLoopIndex;    } set { m_beginLoopIndex    = value; } }    // Index of associated BEGIN_LOOP command - valid for END_LOOP commands
        public  int             WaitKey         { get { return m_waitKey;           } set { m_waitKey           = value; } }    // ID of key to be pressed (?) - valid for WAIT_KEY commands

        private DLCommandType   m_cmdType = DLCommandType.UNKNOWN;
        private Bitmap          m_image = null;
        private uint            m_cmdCode = 0;
        private int             m_imageIndex = -1;
        private int             m_frameIndex = -1;
        private int             m_loopCount = 0;
        private int             m_origLoopCount = 0;
        private int             m_waitKey = 0;
        private int             m_beginLoopIndex = 0;
    }

}
