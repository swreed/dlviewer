
// ===================================================================
//
//  DLViewerSettings.cs
//
//  Container for configuration settings for DLViewer application.
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DLViewer
{
    public class DLViewerSettings
    {
        public DLViewerSettings() { Init(); }

        public string               PrevDirectoryName           { get; set; }   // Most recently used directory name
        public bool                 RepeatAnimationFlag         { get; set; }   // Flag: At end of animation, restart from beginning
        public int                  FrameIntervalMilliseconds   { get; set; }   // Delay between frames in milliseconds
        public PictureBoxSizeMode   ImageResizeMode             { get; set; }   // Animation Display: Actual Size or Zoomed
        public string               FilenameWildcard            { get; set; }   // Directory wildcard used to load DL files in viewer window

        public const bool               DEFAULT_REPEAT_ANIMATION    = true;
        public const int                DEFAULT_FRAME_INTERVAL      = 100;      // 100 Thousandths of a second = .1 second
        public const string             DEFAULT_DIRECTORY_WILDCARD  = "*.dl";   // Assume DL animation files will have a .DL extension

        public const PictureBoxSizeMode DEFAULT_IMAGE_RESIZE_MODE = PictureBoxSizeMode.Zoom;  // PictureBoxSizeMode.Zoom or PictureBoxSizeMode.CenterImage
        
        // Key names for settings
        private const string PREV_DIRECTORY_NAME_KEY            = "PrevDirectoryName";
        private const string REPEAT_ANIMATION_FLAG_KEY          = "RepeatAnimationFlag";
        private const string IMAGE_RESIZE_MODE_KEY              = "ImageResizeMode";
        private const string FRAME_INTERVAL_MILLISECONDS_KEY    = "FrameIntervalMilliseconds";
        private const string FILENAME_WILDCARD_KEY              = "FilenameWildcard";

        /// <summary>
        /// Init - Initialize class members with default values
        /// </summary>
        
        private void Init()
        {
            this.PrevDirectoryName          = string.Empty;
            this.RepeatAnimationFlag        = DEFAULT_REPEAT_ANIMATION;
            this.FrameIntervalMilliseconds  = DEFAULT_FRAME_INTERVAL;
            this.ImageResizeMode            = DEFAULT_IMAGE_RESIZE_MODE;
            this.FilenameWildcard           = DEFAULT_DIRECTORY_WILDCARD;
        }

        /// <summary>
        /// Load - Load configuration settings
        /// </summary>

        public void Load()
        {
            this.PrevDirectoryName          = (string)Properties.Settings.Default[PREV_DIRECTORY_NAME_KEY];
            this.RepeatAnimationFlag        = (bool)Properties.Settings.Default[REPEAT_ANIMATION_FLAG_KEY];
            this.FrameIntervalMilliseconds  = (int)Properties.Settings.Default[FRAME_INTERVAL_MILLISECONDS_KEY];
            this.FilenameWildcard           = (string)Properties.Settings.Default[FILENAME_WILDCARD_KEY];

            string value = (string)Properties.Settings.Default[IMAGE_RESIZE_MODE_KEY];

            this.ImageResizeMode = string.IsNullOrEmpty(value) ? DEFAULT_IMAGE_RESIZE_MODE : (PictureBoxSizeMode)Enum.Parse(typeof(PictureBoxSizeMode), value);
        }

        /// <summary>
        /// Save - Save configuration settings
        /// </summary>

        public void Save()
        {
            Properties.Settings.Default[PREV_DIRECTORY_NAME_KEY]            = this.PrevDirectoryName;
            Properties.Settings.Default[REPEAT_ANIMATION_FLAG_KEY]          = this.RepeatAnimationFlag;
            Properties.Settings.Default[FRAME_INTERVAL_MILLISECONDS_KEY]    = this.FrameIntervalMilliseconds;
            Properties.Settings.Default[IMAGE_RESIZE_MODE_KEY]              = this.ImageResizeMode.ToString();
            Properties.Settings.Default[FILENAME_WILDCARD_KEY]              = this.FilenameWildcard;

            Properties.Settings.Default.Save();
        }
    }
}
