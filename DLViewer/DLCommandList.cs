﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLViewer
{
    /// <summary>
    /// DLCommandList - Container for series of DLCommands
    /// </summary>

    public class DLCommandList : List<DLCommand>
    {
        public DLCommandList() : base() { }

        // Counts of the various types of commands in the list
        public int BeginLoopCmdCount    { get; private set; }
        public int EndLoopCmdCount      { get; private set; }
        public int MaxLoopCount         { get; private set; }
        public int WaitKeyCmdCount      { get; private set; }
        public int ShowImageCmdCount    { get; private set; }
        public int UnknownCmdCount      { get; private set; }

        // Flag: When we get to last command, should we restart at the beginning?
        public bool RestartAtEnd        { get; set; }

        private int m_currentCmdIndex = -1;

        // Loop context stack - used by GetNextCommand() to track state of nested loops
        private Stack<DLLoopContext> m_loopContext = new Stack<DLLoopContext>();

        // Begin loop index stack - used by Add() method to track nested loops
        private Stack<int> m_beginLoopIndexes = new Stack<int>();

        public new void Clear()
        {
            base.Clear();

            this.BeginLoopCmdCount = 0;
            this.EndLoopCmdCount = 0;
            this.MaxLoopCount = 0;
            this.WaitKeyCmdCount = 0;
            this.ShowImageCmdCount = 0;
            this.UnknownCmdCount = 0;

            m_currentCmdIndex = -1;
            m_loopContext.Clear();
            m_beginLoopIndexes.Clear();
        }

        public void MoveToFirstCommand()
        {
            m_currentCmdIndex = -1;
            m_loopContext.Clear();
        }

        public void MoveToLastCommand()
        {
            if (this.Count < 2)
                MoveToFirstCommand();
            else
            {
                m_currentCmdIndex = (this.Count - 2);
                m_loopContext.Clear();
            }
        }

        public DLCommand GetFirstCommand()
        {
            MoveToFirstCommand();

            return GetNextCommand();
        }

        public DLCommand GetLastCommand()
        {
            MoveToLastCommand();

            return GetNextCommand();
        }

        /// <summary>
        /// Get next DL Command in our list.
        /// If we reach the end of the list, it starts over
        /// with the first command. It may make recursive
        /// calls to handle looping.
        /// </summary>
        /// <returns></returns>

        public DLCommand GetNextCommand()
        {
            if (this.Count < 1)
                return null;

            m_currentCmdIndex++;

            if (m_currentCmdIndex >= this.Count)
            {
                // We've reach the end of the command list.

                if (this.RestartAtEnd)
                {
                    // Restart at the beginning.
                    return GetFirstCommand();
                }
                else
                {
                    return null;
                }
            }

            DLCommand cmd = this[m_currentCmdIndex];

            if (cmd.CommandType == DLCommandType.BEGIN_LOOP)
            {
                // We're starting a loop...

                // Create a new LoopContext instance to track this loop's state
                DLLoopContext ctx = new DLLoopContext(m_currentCmdIndex, cmd.LoopCount);

                // Add it to the stack
                m_loopContext.Push(ctx);

                // Return the next command in the loop
                return GetNextCommand();
            }
            else if (cmd.CommandType == DLCommandType.END_LOOP)
            {
                // We've reached the end of a loop.

                // If there is no matching BEGIN LOOP on the stack,
                // something is wrong. Just return null;
                if (m_loopContext == null || m_loopContext.Count < 1)
                    return null;

                DLLoopContext ctx = m_loopContext.Peek();

                // Increment the loop count.
                ctx.IncrementLoopCount();

                // Have we performed the loop the required number of times?
                if (!ctx.KeepLooping())
                {
                    // We've finished looping. Remove the loop state from the stack and
                    // continue with the next command.
                    m_loopContext.Pop();
                }
                else
                {
                    // We're not done looping. Go back to the command at the start of the loop.
                    m_currentCmdIndex = ctx.BeginLoopIndex;
                }

                // Recursive call to get next non-loop command
                return GetNextCommand();
            }

            return cmd;
        }

        /// <summary>
        /// Add a command to the list.
        /// </summary>
        /// <param name="cmd"></param>

        public new void Add(DLCommand cmd)
        {
            switch(cmd.CommandType)
            {
                // If this is a new END_LOOP command, find
                // its corresponding BEGIN_LOOP (which 
                // should already be in this list)

                case DLCommandType.END_LOOP :
                {
                    if (m_beginLoopIndexes.Count < 1)
                    {
                        // This END_LOOP command has no matching BEGIN_LOOP...
                        // Just skip it...
                        return;
                    }

                    cmd.BeginLoopIndex = m_beginLoopIndexes.Peek();

                    // We've matched our most recent begin loop
                    m_beginLoopIndexes.Pop();

                    // Check for empty loop
                    if ((this.Count - cmd.BeginLoopIndex) < 2)
                    {
                        // This is an empty loop consisting of just a BEGIN LOOP and END LOOP command...

                        // Remove useless BEGIN LOOP command
                        this.RemoveAt(cmd.BeginLoopIndex);
                        this.BeginLoopCmdCount--;

                        // Discard END LOOP command
                        return;
                    }

                    this.EndLoopCmdCount++;

                    break;
                }

                // If this is a new BEGIN_LOOP command,
                // save its list index so we can link it
                // to its corresponding END_LOOP command

                case DLCommandType.BEGIN_LOOP:
                {
                    if (cmd.LoopCount > this.MaxLoopCount)
                        this.MaxLoopCount = cmd.LoopCount;

                    // Sanity check: limit the number of times
                    // the loop will repeat...
                    const int MAX_LOOP_COUNT = 20;

                    cmd.LoopCount = Math.Max(1, Math.Min(cmd.LoopCount, MAX_LOOP_COUNT));

                    // Add this BEGIN_LOOP list index to the stack
                    m_beginLoopIndexes.Push(this.Count);

                    this.BeginLoopCmdCount++;

                    break;
                }

                case DLCommandType.WAIT_KEY:
                {
                    this.WaitKeyCmdCount++;
                    break;
                }

                case DLCommandType.SHOW_IMAGE:
                {
                    this.ShowImageCmdCount++;
                    break;
                }

                default :
                {
                    cmd.CommandType = DLCommandType.UNKNOWN;
                    this.UnknownCmdCount++;
                    break;
                }
            }

            base.Add(cmd);
        }
    }

    /// <summary>
    /// DLLoopContext - Tracks DL Loop Command state
    /// Used to track loop count and BEGIN_LOOP index for a series of looped commands
    /// </summary>

    internal class DLLoopContext
    {
        public DLLoopContext(int beginIndex, int maxCount)
        {
            this.BeginLoopIndex = beginIndex;
            this.MaxLoopCount = maxCount;
            this.LoopCount = 0;
        }

        public void IncrementLoopCount()
        {
            this.LoopCount++;
        }

        public bool KeepLooping()
        {
            return (this.LoopCount < this.MaxLoopCount);
        }

        public int BeginLoopIndex { get; private set; }
        public int MaxLoopCount   { get; private set; }
        public int LoopCount      { get; private set; }
    }
}
