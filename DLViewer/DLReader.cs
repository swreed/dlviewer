﻿
// ===================================================================
//
//  DLReader.cs - DL Animation file reader
//
//  This software is released under the MIT License (MIT):
//
//  Copyright © 2015 400Things.com
//
//  Permission is hereby granted, free of charge, to any person obtaining 
//  a copy of this software and associated documentation files (the 
//  "Software"), to deal in the Software without restriction, including 
//  without limitation the rights to use, copy, modify, merge, publish, 
//  distribute, sublicense, and/or sell copies of the Software, 
//  and to permit persons to whom the Software is furnished to do so, 
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included 
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
//  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
//  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR 
//  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
//  ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
//  OTHER DEALINGS IN THE SOFTWARE.
//
//  Sample usage:
//
//  // Read DL file
//  DLReader reader = new DLReader("c:\\myFile.dl");
//
//  string title = reader.Title;
//  string authorName = reader.AuthorName;
//
//  // Get first command from file
//  DLCommand cmd = reader.GetFirstCommand();
//
//  while(cmd != null)
//  {
//      // Commands types include SHOW_IMAGE and WAIT_KEY...
//
//      if (cmd.CommandType == DLCommandType.SHOW_IMAGE)
//      {
//          // Display the image
//          DoSomething(cmd.Image);
//      }
//
//      // Get next command from file
//      cmd = reader.GetNextCommand();
//  }
//
// ===================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

using Gif.Components;  // Animated GIF Encoding

namespace DLViewer
{
    /// <summary>
    /// DLReader class - Reads 1990s-era .DL animation files
    /// </summary>

    public class DLReader
    {
        /// <summary>
        /// DLReader - default constructor
        /// </summary>
        
        public DLReader() 
        { 
            Init(); 
        }

        /// <summary>
        /// DLReader(string fileName)
        /// Constructor that takes a filename
        /// </summary>
        /// <param name="fileName"></param>

        public DLReader(string fileName)
        {
            Init();

            Load(fileName);
        }

        public string   FileName            { get; private set; }
        public uint     FileLength          { get; private set; }
        public int      FileVersion         { get; private set; }
        public int      FileFormat          { get; private set; }
        public int      ImagesPerScreen     { get; private set; }
        public int      NumberOfScreens     { get; private set; }
        public int      NumberOfFrames      { get; private set; }
        public int      NumberOfImages      { get; private set; }
        public int      NumberOfSounds      { get; private set; }

        public int      ImageCount          { get { return (m_images != null) ? m_images.Length : 0; } }        // Number of unique images in the animation
        public int      ScreenCount         { get { return (m_screens != null) ? m_screens.Length : 0; } }      // Number of 320x200 screens read from DL file that contain the images images
        public int      FrameCount          { get { return m_frameIndexes.Count; } }                            // Number of animation frames - this is a (possibly repeating) series of image indexes

        public string   Title               { get; private set; }   // Title of animation
        public string   AuthorName          { get; private set; }   // Author's Name (found in version 2 and 3 DL animation files)

        private int     m_imageX;   // Width of each image in pixels
        private int     m_imageY;   // Height of each image in pixels

        private Color[] m_palette = new Color[DLConstants.DL_MAX_COLORS];  // Color palette (256 colors)

        private Bitmap[] m_screens = null;  // Array of animation screens - each screen will contain from 1 to 16 images
        private Bitmap[] m_images = null;   // Array of animation images extracted from screens

        private List<Size> m_screenSizes = new List<Size>();  // List of screen sizes - for DL version 3, each screen may have a different size

        private List<int> m_frameIndexes = new List<int>();  // List of frame indexes - this is a sequence of (possibly repeating) image indexes that make the animation

        private DLCommandList m_commandList = new DLCommandList();

        private List<byte[]> m_sounds = new List<byte[]>();

        public int CommandCount         { get { return m_commandList.Count; } }
        public int BeginLoopCmdCount    { get { return m_commandList.BeginLoopCmdCount; } }
        public int EndLoopCmdCount      { get { return m_commandList.EndLoopCmdCount; } }
        public int MaxLoopCount         { get { return m_commandList.MaxLoopCount;  } }
        public int WaitKeyCmdCount      { get { return m_commandList.WaitKeyCmdCount; } }
        public int ShowImageCmdCount    { get { return m_commandList.ShowImageCmdCount; } }
        public int UnknownCmdCount      { get { return m_commandList.UnknownCmdCount; } }

        // When animation reaches last frame, should it repeat from the beginning?
        public bool RepeatAnimation { get { return m_commandList.RestartAtEnd; } set { m_commandList.RestartAtEnd = value; } }

        /// <summary>
        /// Init - initialize member vars.
        /// Called from constructors.
        /// </summary>

        private void Init()
        {
            this.AuthorName = string.Empty;
            this.Title      = string.Empty;
            this.FileName   = string.Empty;
        }

        /// <summary>
        /// Clear - Reset this instance
        /// </summary>

        public void Clear()
        {
            this.AuthorName = string.Empty;
            this.Title = string.Empty;
            this.FileName = string.Empty;

            this.FileFormat = 0;
            this.FileLength = 0;
            this.FileVersion = 0;
            this.ImagesPerScreen = 0;
            
            this.m_imageX = 0;
            this.m_imageY = 0;

            this.m_images = null;
            this.m_screens = null;
            this.m_screenSizes.Clear();

            this.m_frameIndexes.Clear();
            this.m_commandList.Clear();

            this.NumberOfSounds = 0;
            this.m_sounds.Clear();
        }

        /// <summary>
        /// GetNextCommand
        /// </summary>
        /// <returns></returns>

        public DLCommand GetNextCommand()
        {
            return m_commandList.GetNextCommand();
        }

        public void MoveToFirstCommand()
        {
            m_commandList.MoveToFirstCommand();
        }

        public void MoveToLastCommand()
        {
            m_commandList.MoveToLastCommand();
        }

        /// <summary>
        /// GetGIFComment
        /// Formats DL Title and Author strings as a text
        /// string suitable for inclusion in a GIF image
        /// comment block. Called when converting DL to an
        /// animated GIF image.
        /// </summary>
        /// <returns></returns>

        private string GetGIFComment()
        {
            string comment = string.Empty;

            if (!string.IsNullOrEmpty(this.Title))
                comment = string.Format("Title: {0}", this.Title);

            if (!string.IsNullOrEmpty(this.AuthorName))
            {
                if (comment.Length > 0)
                    comment += " ";

                comment += string.Format("Author: {0}", this.AuthorName);
            }

            return comment;
        }

        /// <summary>
        /// SaveBitmapsToGIF
        /// Saves DL Bitmaps to an animated GIF image
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="bitmapArray"></param>

        private void SaveBitmapsToGIF(string fileName, Bitmap[] bitmapArray)
        {
            if (bitmapArray == null || bitmapArray.Length < 1)
                return;

            AnimatedGifEncoder e = new AnimatedGifEncoder();

            e.Start(fileName);

            e.SetDelay(100);
            e.SetRepeat(0);  //-1:no repeat,0:always repeat

            e.SetComment(GetGIFComment());

            for (int i = 0; i < bitmapArray.Length; i++)
            {
                e.AddFrame(bitmapArray[i]);
            }

            e.Finish();
        }

        /// <summary>
        /// SaveScreensToGIF
        /// </summary>
        /// <param name="fileName"></param>

        public void SaveScreensToGIF(string fileName)
        {
            SaveBitmapsToGIF(fileName, this.m_screens);
        }

        /// <summary>
        /// SaveImagesToGIF
        /// </summary>
        /// <param name="fileName"></param>

        public void SaveImagesToGIF(string fileName)
        {
            SaveBitmapsToGIF(fileName, this.m_images);
        }

        /// <summary>
        /// SaveFramesToGIF
        /// Write the DL frame images to an animated GIF file.
        /// 
        /// NOTE: the resulting GIF may differ slightly
        /// from the DL file. All images will be
        /// output, but in order to keep the output file down
        /// to a reasonable size, repeating loops sequences 
        /// are not written to the GIF. The resulting GIF 
        /// animation will run as if the DL file had no
        /// loops.
        /// </summary>
        /// <param name="fileName"></param>

        public void SaveFramesToGIF(string fileName)
        {
            if (this.m_images == null || this.m_images.Length < 1)
                return;

            AnimatedGifEncoder e = new AnimatedGifEncoder();

            e.Start(fileName);

            e.SetDelay(100);
            e.SetRepeat(0);  //-1:no repeat,0:always repeat

            e.SetComment(GetGIFComment());

            for (int i = 0; i < this.m_frameIndexes.Count; i++)
            {
                e.AddFrame(m_images[m_frameIndexes[i]]);
            }

            e.Finish();
        }

        /// <summary>
        /// Read the Title or AuthorName field from the DL file.
        /// These are either 20 (v1/v2) or 40 (v3) characters longs.
        /// The character values are XOR'd with 0xff.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="sizeInBytes"></param>
        /// <returns></returns>

        private string ReadTextField(BinaryReader reader, int sizeInBytes)
        {
            StringBuilder sb = new StringBuilder(sizeInBytes);

            byte[] textVal = reader.ReadBytes(sizeInBytes);

            for (int i = 0; i < sizeInBytes; i++)
            {
                // In DL text fields, the character values are XOR'd with 0xff...
                byte b = (byte)((uint)textVal[i] ^ (uint)0xff);

                if (b < (byte)0x80)
                {
                    sb.Append((char)b);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// SetFormat
        /// Set DL image parameters based on the given file format version.
        /// </summary>
        /// <param name="format"></param>

        private void SetFormat(int format)
        {
            switch (format)
            {
                case DLConstants.DL_FORM_LARGE:
                    this.FileFormat = format;
                    this.m_imageX   = DLConstants.DL_LARGE_XSIZE;
                    this.m_imageY   = DLConstants.DL_LARGE_YSIZE;
                    this.ImagesPerScreen = 1;
                    break;

                case DLConstants.DL_FORM_MEDIUM:
                    this.FileFormat = format;
                    this.m_imageX   = DLConstants.DL_MEDIUM_XSIZE;
                    this.m_imageY   = DLConstants.DL_MEDIUM_YSIZE;
                    this.ImagesPerScreen = 4;
                    break;

                case DLConstants.DL_FORM_SMALL:
                    this.FileFormat = format;
                    this.m_imageX = DLConstants.DL_SMALL_XSIZE;
                    this.m_imageY = DLConstants.DL_SMALL_YSIZE;
                    this.ImagesPerScreen = 16;
                    break;

                default:
                    throw new ArgumentException(string.Format("File {0} contains unexpected DL Format Value {1} : Expected values {2}, {3} or {4}.", this.FileName, format, DLConstants.DL_FORM_LARGE, DLConstants.DL_FORM_MEDIUM, DLConstants.DL_FORM_SMALL));
            }
        }

        /// <summary>
        /// LoadPalette
        /// Load 256-color palette used by DL screens.
        /// 
        /// Note that DL files use a VGA color palette
        /// that have an intensity range of (0-63) per
        /// RGB color component; modern displays generally
        /// have an intensity of range (0-255), so we need
        /// to scale the DL palette so it looks correct
        /// on modern displays; otherwise the DL image will
        /// look too dark.
        /// </summary>
        /// <param name="reader"></param>

        private void LoadPalette(BinaryReader reader)
        {
            /*
            To calculate a VGA color value, select the intensities of red, green, 
            and blue. The intensity of a color is a number from 0 (low intensity) 
            to 63 (high intensity). Then use the following formula to calculate 
            the actual color number: 
  
                color number = 65536 * blue + 256 * green + red 
  
            This formula yields integer values from 0 to 4,144,959, but 
            because there are gaps in the range of color numbers, you should 
            use the formula rather than just select a number.
             */

            // VGA color values (Red,Green,Blue) range from 0-63. We need to scale them 
            // up to a range of 0-255, so we'll just multiply each color value by 4.
            const int SCALING_FACTOR = 4;

            for (int i = 0; i < DLConstants.DL_MAX_COLORS; i++)
            {
                int red   = (((int)reader.ReadByte()) * SCALING_FACTOR) & 0xff;
                int green = (((int)reader.ReadByte()) * SCALING_FACTOR) & 0xff;
                int blue =  (((int)reader.ReadByte()) * SCALING_FACTOR) & 0xff;

                m_palette[i] = Color.FromArgb(red, green, blue);
            }
        }

        /// <summary>
        /// LoadScreens
        /// </summary>
        /// <param name="reader"></param>

        private void LoadScreens(BinaryReader reader)
        {
            m_screens = new Bitmap[this.NumberOfScreens];

            for (int i = 0; i < this.NumberOfScreens; i++)
            {
                m_screens[i] = LoadScreen(DLConstants.DL_LARGE_XSIZE, DLConstants.DL_LARGE_YSIZE, reader);
            }

            LoadImages();
        }

        /// <summary>
        /// LoadScreen
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="reader"></param>
        /// <returns></returns>

        private Bitmap LoadScreen(int width, int height, BinaryReader reader)
        {
            if (this.FileVersion == DLConstants.DL_VERSION_3)
            {
                width = (int)reader.ReadUInt16();
                height = (int)reader.ReadUInt16();
            }

            m_screenSizes.Add(new Size(width, height));

            Bitmap screen = new Bitmap(width, height);

            try
            {
                // DL images are 256 colors, 8 bits (one byte) per pixel,
                // so a 320x200 image will require 64000 bytes.
                int numBytes = width * height;

                // Read the image data from the file
                byte[] data = reader.ReadBytes(numBytes);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        int index = (y * width) + x;

                        byte b = data[index];

                        screen.SetPixel(x, y, m_palette[b]);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO: Error handling here!!!
                throw new ApplicationException("Error while reading screen data from DL file", e);
            }

            return screen;
        }

        /// <summary>
        /// LoadImages
        /// </summary>

        private void LoadImages()
        {
            m_images = new Bitmap[this.NumberOfImages];

            int imagesToLoad = this.NumberOfImages;
            int totalImages = 0;

            for (int screen = 0; screen < m_screens.Length; screen++)
            {
                int imageX = (this.FileVersion == DLConstants.DL_VERSION_3) ? m_screenSizes[screen].Width : m_imageX;
                int imageY = (this.FileVersion == DLConstants.DL_VERSION_3) ? m_screenSizes[screen].Height : m_imageY;

                int imagesToLoadFromScreen = (imagesToLoad >= this.ImagesPerScreen) ? this.ImagesPerScreen : imagesToLoad;

                for (int image = 0; image < imagesToLoadFromScreen; image++)
                {
                    Bitmap img = LoadImage(imageX, imageY, image, m_screens[screen]);

                    m_images[totalImages++] = img;
                }

                imagesToLoad -= imagesToLoadFromScreen;
            }
        }

        /// <summary>
        /// LoadImage
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="index"></param>
        /// <param name="sourceScreen"></param>
        /// <returns></returns>

        private Bitmap LoadImage(int width, int height, int index, Bitmap sourceScreen)
        {
            int bitmapWidth = sourceScreen.Width;
            int bitmapHeight = sourceScreen.Height;

            int xperscreen = bitmapWidth / width;

            int xoffset = (index % xperscreen) * width;
            int yoffset = (index / xperscreen) * height;

            Bitmap image = new Bitmap(width, height);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int sourceX = xoffset + x;
                    int sourceY = yoffset + y;

                    image.SetPixel(x, y, sourceScreen.GetPixel(sourceX, sourceY));
                }
            }

            return image;
        }

        /// <summary>
        /// LoadFramesV2V3
        /// Load version 2 & 3 frame/command data from DL file
        /// </summary>
        /// <param name="reader"></param>

        private void LoadFramesV2V3(BinaryReader reader)
        {
            m_frameIndexes.Clear();

            m_commandList.Clear();

            int i = 0;

            DLFileCommand fileCmd = null;
            DLFileCommand prevCmd = null;

            while (i < this.NumberOfFrames)
            {
                uint frameCmd = (uint)reader.ReadUInt16();

                i++;

                if (frameCmd >= DLConstants.DL_CMD_MAX_IMAGE)
                {
                    prevCmd = fileCmd;

                    fileCmd = DLFileCommand.GetCommand(frameCmd);

                    if (fileCmd == null)
                    {
                        string prevCmdName = (prevCmd == null) ? "NULL" : prevCmd.Name;
                        System.Diagnostics.Debug.WriteLine("ERROR: Unknown command value {0}. Prev Command = {1}", frameCmd, prevCmdName);
                    }

                    switch (frameCmd)
                    {
                        case DLConstants.DL_CMD_BEGIN_LOOP :
                        {
                            // 0xffff : Begin Loop

                            int loopCount = (int)reader.ReadUInt16();

                            i++;

                            DLCommand cmd = new DLCommand(DLCommandType.BEGIN_LOOP);
                            cmd.CommandCode = frameCmd;
                            cmd.LoopCount = loopCount;
                            cmd.OrigLoopCount = loopCount;
                            m_commandList.Add(cmd);

                            break;
                        }

                        case DLConstants.DL_CMD_END_LOOP:
                        {
                            // 0xffff : End Loop

                            DLCommand cmd = new DLCommand(DLCommandType.END_LOOP);
                            cmd.CommandCode = frameCmd;
                            m_commandList.Add(cmd);

                            break;
                        }

                        case DLConstants.DL_CMD_WAIT_KEY:
                        {
                                // 0xfffd : Wait for Key

                            //int key = (int)reader.ReadUInt16();
                            int key = -1;

                            DLCommand cmd = new DLCommand(DLCommandType.WAIT_KEY);
                            cmd.CommandCode = frameCmd;
                            cmd.WaitKey = key;
                            m_commandList.Add(cmd);

                            i++;

                            break;
                        }

                        case DLConstants.DL_CMD_CLS :
                        {
                            // 0xfff4 : Clear the screen

                            DLCommand cmd = new DLCommand(DLCommandType.CLEAR_SCREEN);
                            cmd.CommandCode = frameCmd;
                            m_commandList.Add(cmd);

                            i++;

                            break;
                        }

                        default:
                        {
                            // Unrecognized command code...
                            break;
                        }
                    }
                }
                else
                {
                    // Cmd is less than 0x8000 - Show an image
                    int imageIndex = (int)frameCmd;

                    if (imageIndex >= 0 && imageIndex < this.m_images.Length)
                    {
                        DLCommand cmd = new DLCommand(DLCommandType.SHOW_IMAGE);
                        cmd.CommandCode = frameCmd;
                        cmd.ImageIndex = imageIndex;
                        cmd.FrameIndex = m_frameIndexes.Count;
                        cmd.Image = m_images[imageIndex];
                        m_commandList.Add(cmd);

                        m_frameIndexes.Add((int)imageIndex);
                    }
                    else
                    {
                        // Invalid image ID...
                    }
                }
            }
        }

        /// <summary>
        /// LoadV1 - Load DL version 1 file
        /// </summary>
        /// <param name="reader"></param>

        private void LoadV1(BinaryReader reader)
        {
            SetFormat(DLConstants.DL_FORM_MEDIUM);

            // Read file title (20 chars)
            this.Title = ReadTextField(reader, DLConstants.DL_TEXT_LENGTH_V1);

            // Version 1 doesn't support AuthorName
            this.AuthorName = string.Empty;

            int screenCount = (int)reader.ReadByte();

            if (screenCount < 1 || screenCount >= DLConstants.DL_CMD_MAX_IMAGE)
                throw new ArgumentException(string.Format("DL file {0} contains invalid Screen Count value {1} : Expected value < {2}.", this.FileName, screenCount, DLConstants.DL_CMD_MAX_IMAGE));

            int frameCount = (int)reader.ReadUInt16();

            if (frameCount < 1 || screenCount >= DLConstants.DL_CMD_MAX_IMAGE)
                throw new ArgumentException(string.Format("DL file {0} contains invalid Frame Count value {1}.", this.FileName, frameCount));

            this.NumberOfScreens = screenCount;

            this.NumberOfImages = this.NumberOfScreens * this.ImagesPerScreen;

            this.NumberOfFrames = frameCount;

            this.NumberOfScreens = screenCount;

            // Load color palette
            LoadPalette(reader);

            // Load screens and images
            LoadScreens(reader);

            // Load frame commands.
            // Version 1 files simply display a series of images.
            // There is no support for looping or keypresses

            m_frameIndexes.Clear();
            m_commandList.Clear();

            for (int i = 0; i < this.NumberOfFrames; i++)
            {
                int imageIndex = (int)reader.ReadByte();

                imageIndex = (imageIndex % 10) - 1 + ((imageIndex / 10) - 1) * 4;

                if (imageIndex >= 0 && imageIndex < this.NumberOfImages)
                {
                    m_frameIndexes.Add(imageIndex);

                    DLCommand cmd = new DLCommand(DLCommandType.SHOW_IMAGE);
                    cmd.Image = m_images[imageIndex];
                    m_commandList.Add(cmd);
                }
            }
        }

        /// <summary>
        /// LoadV2 - Load DL version 2 file
        /// (Seems to be the most common file version...)
        /// </summary>
        /// <param name="reader"></param>

        private void LoadV2(BinaryReader reader)
        {
            SetFormat((int)reader.ReadByte());

            // Read file title - 20 chars
            this.Title = ReadTextField(reader, DLConstants.DL_TEXT_LENGTH_V2);

            // Read file author - 20 chars
            this.AuthorName = ReadTextField(reader, DLConstants.DL_TEXT_LENGTH_V2);

            int screenCount = (int)reader.ReadByte();

            if (screenCount < 0 || screenCount >= DLConstants.DL_CMD_MAX_IMAGE)
                throw new ArgumentException(string.Format("DL file {0} contains invalid Screen Count value {1} : Expected value < {2}.", this.FileName, screenCount, DLConstants.DL_CMD_MAX_IMAGE));

            this.NumberOfScreens = screenCount;

            this.NumberOfFrames = reader.ReadInt32();

            this.NumberOfImages = this.NumberOfScreens * this.ImagesPerScreen;

            // Load color palette (256 colors)
            LoadPalette(reader);

            // Load screens and images
            LoadScreens(reader);

            // Load frame commands : SHOW_IMAGE, BEGIN_LOOP, etc
            LoadFramesV2V3(reader);
        }

        /// <summary>
        /// LoadV3 - Load DL version 3 file
        /// In version 3, the file format changed somewhat.
        /// Text fields are longer, images can be different sizes,
        /// and audio is supported (but not supported in this viewer...)
        /// </summary>
        /// <param name="reader"></param>

        private void LoadV3(BinaryReader reader)
        {
            // skip format byte - version 3 always uses large format
            reader.ReadByte();
            SetFormat(DLConstants.DL_FORM_LARGE);

            // skip over undocumented v3 header bytes...
            const int V3_UNDOCUMENTED_HEADER_SIZE = 50;
            byte[] unknown = reader.ReadBytes(V3_UNDOCUMENTED_HEADER_SIZE);

            // Read file title (40 chars in v3)
            this.Title = ReadTextField(reader, DLConstants.DL_TEXT_LENGTH_V3);

            // Read file author (40 chars in v3)
            this.AuthorName = ReadTextField(reader, DLConstants.DL_TEXT_LENGTH_V3);

            // Read number of screens (16-bit int in v3)
            this.NumberOfScreens = (int)reader.ReadInt16();

            this.NumberOfImages = this.NumberOfScreens * this.ImagesPerScreen;

            // Read number of frames
            this.NumberOfFrames = (int)reader.ReadInt16();

            // DL Version 3 supports sound (but this class doesn't...)
            this.NumberOfSounds = (int)reader.ReadInt16();

            // Load color palette (256 colors)
            LoadPalette(reader);

            // Load the screens (320x200) and images
            LoadScreens(reader);

            // Load frame commands : SHOW_IMAGE, BEGIN_LOOP, END_LOOP, etc
            LoadFramesV2V3(reader);

            LoadSounds(reader);
        }

        private void LoadSounds(BinaryReader reader)
        {
            if (this.FileVersion != DLConstants.DL_VERSION_3 || this.NumberOfSounds < 1)
                return;

            const uint MAX_SOUND_SIZE = (64000 * 4);

            for (int i = 0; i < this.NumberOfSounds; i++)
            {
                uint dimension = reader.ReadUInt32();

                if (dimension < 1 || dimension > MAX_SOUND_SIZE)
                    return;

                uint soundType = (uint)reader.ReadByte();

                byte[] sample = reader.ReadBytes((int)dimension);

                this.m_sounds.Add(sample);
            }
        }

        /// <summary>
        /// Load DL animation from a File
        /// </summary>
        /// <param name="fileName"></param>

        public void Load(string fileName)
        {
            FileInfo fi = new FileInfo(fileName);

            if (!fi.Exists)
                throw new FileNotFoundException(string.Format("File {0} not found.", fileName));

            this.FileName = fi.Name;
            this.FileLength = (uint)fi.Length;

            // Open the file and read its contents
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                Load(fs);
            }
        }

        /// <summary>
        /// Load DL animation from a Stream
        /// </summary>
        /// <param name="stream"></param>

        public void Load(Stream stream)
        {
            if (stream == null || !stream.CanRead)
                throw new IOException("Unable to read DL animation");

            using (BinaryReader reader = new BinaryReader(stream))
            {
                // First byte of the DL file contains file version.
                // We recognize versions 1, 2 or 3...

                this.FileVersion = (int)reader.ReadByte();

                switch (this.FileVersion)
                {
                    // Load Version 1 file...
                    case DLConstants.DL_VERSION_1:
                        LoadV1(reader);
                        break;

                    // Load Version 2 file...
                    case DLConstants.DL_VERSION_2:
                        LoadV2(reader);
                        break;

                    // Load Version 3 file...
                    case DLConstants.DL_VERSION_3:
                        LoadV3(reader);
                        break;

                    // Unrecognized version number.
                    default:
                        throw new ArgumentException(string.Format("File {0} contains unrecognized DL Version Number : Found {1}, expected either {2}, {3} or {4}.", this.FileName, this.FileVersion, DLConstants.DL_VERSION_1, DLConstants.DL_VERSION_2, DLConstants.DL_VERSION_3));

                }  // switch

            }  // using (BinaryReader...

        } // void Load()...

    } // class DLAnimation

}  // namespace DLViewer
