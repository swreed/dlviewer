﻿namespace DLViewer
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbActualSize = new System.Windows.Forms.RadioButton();
            this.rbResize = new System.Windows.Forms.RadioButton();
            this.gbFrameDelay = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.udFrameDelay = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxRepeat = new System.Windows.Forms.CheckBox();
            this.cbOK = new System.Windows.Forms.Button();
            this.cbCancel = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.gbFrameDelay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFrameDelay)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbActualSize);
            this.groupBox1.Controls.Add(this.rbResize);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 51);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Animation Size";
            // 
            // rbActualSize
            // 
            this.rbActualSize.AutoSize = true;
            this.rbActualSize.Location = new System.Drawing.Point(242, 20);
            this.rbActualSize.Name = "rbActualSize";
            this.rbActualSize.Size = new System.Drawing.Size(108, 17);
            this.rbActualSize.TabIndex = 1;
            this.rbActualSize.TabStop = true;
            this.rbActualSize.Text = "Show Actual Size";
            this.rbActualSize.UseVisualStyleBackColor = true;
            // 
            // rbResize
            // 
            this.rbResize.AutoSize = true;
            this.rbResize.Location = new System.Drawing.Point(48, 20);
            this.rbResize.Name = "rbResize";
            this.rbResize.Size = new System.Drawing.Size(149, 17);
            this.rbResize.TabIndex = 0;
            this.rbResize.TabStop = true;
            this.rbResize.Text = "Resize  to Viewer Window";
            this.rbResize.UseVisualStyleBackColor = true;
            // 
            // gbFrameDelay
            // 
            this.gbFrameDelay.Controls.Add(this.label2);
            this.gbFrameDelay.Controls.Add(this.label1);
            this.gbFrameDelay.Controls.Add(this.udFrameDelay);
            this.gbFrameDelay.Location = new System.Drawing.Point(13, 70);
            this.gbFrameDelay.Name = "gbFrameDelay";
            this.gbFrameDelay.Size = new System.Drawing.Size(398, 50);
            this.gbFrameDelay.TabIndex = 2;
            this.gbFrameDelay.TabStop = false;
            this.gbFrameDelay.Text = "Frame Rate";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(281, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "milliseconds";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Display Next Frame Every";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // udFrameDelay
            // 
            this.udFrameDelay.Location = new System.Drawing.Point(189, 19);
            this.udFrameDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.udFrameDelay.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udFrameDelay.Name = "udFrameDelay";
            this.udFrameDelay.Size = new System.Drawing.Size(84, 20);
            this.udFrameDelay.TabIndex = 0;
            this.udFrameDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udFrameDelay.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.udFrameDelay.ValueChanged += new System.EventHandler(this.udFrameDelay_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbxRepeat);
            this.groupBox2.Location = new System.Drawing.Point(13, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 52);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Repeat";
            // 
            // cbxRepeat
            // 
            this.cbxRepeat.AutoSize = true;
            this.cbxRepeat.Location = new System.Drawing.Point(40, 19);
            this.cbxRepeat.Name = "cbxRepeat";
            this.cbxRepeat.Size = new System.Drawing.Size(318, 17);
            this.cbxRepeat.TabIndex = 0;
            this.cbxRepeat.Text = "When Player Reaches End of Animation, Restart at Beginning";
            this.cbxRepeat.UseVisualStyleBackColor = true;
            // 
            // cbOK
            // 
            this.cbOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cbOK.Location = new System.Drawing.Point(103, 190);
            this.cbOK.Name = "cbOK";
            this.cbOK.Size = new System.Drawing.Size(75, 23);
            this.cbOK.TabIndex = 5;
            this.cbOK.Text = "OK";
            this.cbOK.UseVisualStyleBackColor = true;
            this.cbOK.Click += new System.EventHandler(this.cbOK_Click);
            // 
            // cbCancel
            // 
            this.cbCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cbCancel.Location = new System.Drawing.Point(244, 190);
            this.cbCancel.Name = "cbCancel";
            this.cbCancel.Size = new System.Drawing.Size(75, 23);
            this.cbCancel.TabIndex = 6;
            this.cbCancel.Text = "Cancel";
            this.cbCancel.UseVisualStyleBackColor = true;
            this.cbCancel.Click += new System.EventHandler(this.cbCancel_Click);
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cbCancel;
            this.ClientSize = new System.Drawing.Size(422, 224);
            this.ControlBox = false;
            this.Controls.Add(this.cbCancel);
            this.Controls.Add(this.cbOK);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gbFrameDelay);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DLViewer Settings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbFrameDelay.ResumeLayout(false);
            this.gbFrameDelay.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udFrameDelay)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbActualSize;
        private System.Windows.Forms.RadioButton rbResize;
        private System.Windows.Forms.GroupBox gbFrameDelay;
        private System.Windows.Forms.NumericUpDown udFrameDelay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbxRepeat;
        private System.Windows.Forms.Button cbOK;
        private System.Windows.Forms.Button cbCancel;
    }
}