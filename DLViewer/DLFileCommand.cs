﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLViewer
{
    public class DLFileCommand
    {
        public const uint CYCLE_BEGIN          = 0xFFFF;       // [ Ciclo
        public const uint CYCLE_END            = 0xFFFE;       // ] Ciclo
        public const uint DELAY                = 0xFFFD;       // D Delay     
        public const uint FINE_DL              = 0xFFFC;       // E Fine DL
        public const uint WAIT_AND_EXIT        = 0xFFFB;       // WE Wait e esci
        public const uint WAIT                 = 0xFFFA;       // W wait
        public const uint SPEED_PLUS           = 0xFFF9;       // Vel +
        public const uint SPEED_MINUS          = 0xFFF8;       // Vel -
        public const uint SPEED_MULTIPLY       = 0xFFF7;       // Vel *
        public const uint SPEED_DIVIDE         = 0xFFF6;       // Vel /
        public const uint SPEED_EQUALS         = 0xFFF5;       // Vel =
        public const uint CLS                  = 0xFFF4;       // Cls
        public const uint SOUND_PLAY_ONCE      = 0xFFF3;
        public const uint SOUND_PLAY_LOOP      = 0xFFF2;
        public const uint SHIFT_X_PLUS         = 0xFFF1;       // X+
        public const uint SHIFT_X_MINUS        = 0xFFF0;       // X-
        public const uint SHIFT_Y_PLUS         = 0xFFEF;       // Y+
        public const uint SHIFT_Y_MINUS        = 0xFFEE;       // Y-
        public const uint KEYBOARD_ON          = 0xFFED;       // K+
        public const uint KEYBOARD_OFF         = 0xFFEC;       // K-
        public const uint DL_FILE_NAME         = 0xFFEB;       // #<nome_dl>
        public const uint SET_X_Y              = 0xFFEA;       // (X,Y)
        public const uint SET_XT_YT            = 0xFFE9;       // <X,Y>
        public const uint PRINT_STRING         = 0xFFE8;       // "<stringa>
        public const uint WAIT_KEY             = 0xFFE7;       // Getch
        public const uint SET_COORDS_COLOR     = 0xFFE6;       // VB(...)
        public const uint VB_MINUS             = 0xFFE5;       // VB-
        public const uint VBR_PLUS             = 0xFFE4;       // VBR+
        public const uint VBR_MINUS            = 0xFFE3;       // VBR-
        public const uint MODE_F               = 0xFFE2;       // F
        public const uint MODE_A               = 0xFFE1;       // A
        public const uint MODE_R               = 0xFFE0;       // R
        public const uint MODE_O               = 0xFFDF;       // O
        public const uint PASSWORD             = 0xFFDE;       // Password
        public const uint KEY_PROTECTED        = 0xFFDD;       // @<chiave>,key
        public const uint SHIFT_X              = 0xFFDC;       // X
        public const uint SHIFT_Y              = 0xFFDB;       // Y

        public DLFileCommand()
        {
            this.Value = 0;
            this.Name = String.Empty;
            this.Description = String.Empty;
        }

        public DLFileCommand(uint value, string name, string description)
        {
            this.Value = value;
            this.Name = name;
            this.Description = description;
        }

        public uint     Value           { get; private set; }
        public String   Name            { get; private set; }
        public String   Description     { get; private set; }

        private static Dictionary<uint, DLFileCommand> g_cmdTypes = new Dictionary<uint, DLFileCommand>()
        {
            { CYCLE_BEGIN          , new DLFileCommand(CYCLE_BEGIN      , "CYCLE_BEGIN",    "[ Ciclo")  },
            { CYCLE_END            , new DLFileCommand(CYCLE_END        , "CYCLE_END",      "] Ciclo")  },
            { DELAY                , new DLFileCommand(DELAY            , "DELAY",          "D Delay     ")  },
            { FINE_DL              , new DLFileCommand(FINE_DL          , "FINE_DL",        "E Fine DL")  },
            { WAIT_AND_EXIT        , new DLFileCommand(WAIT_AND_EXIT    , "WAIT_AND_EXIT",  "WE Wait e esci")  },
            { WAIT                 , new DLFileCommand(WAIT             , "WAIT",           "W wait")  },
            { SPEED_PLUS           , new DLFileCommand(SPEED_PLUS       , "SPEED_PLUS",     "Vel +")  },
            { SPEED_MINUS          , new DLFileCommand(SPEED_MINUS      , "SPEED_MINUS",    "Vel -")  },
            { SPEED_MULTIPLY       , new DLFileCommand(SPEED_MULTIPLY   , "SPEED_MULTIPLY", "Vel *")  },
            { SPEED_DIVIDE         , new DLFileCommand(SPEED_DIVIDE     , "SPEED_DIVIDE",   "Vel /")  },
            { SPEED_EQUALS         , new DLFileCommand(SPEED_EQUALS     , "SPEED_EQUALS",   "Vel =")  },
            { CLS                  , new DLFileCommand(CLS              , "CLS",            "Cls")  },
            { SOUND_PLAY_ONCE      , new DLFileCommand(SOUND_PLAY_ONCE  , "SOUND_PLAY_ONCE","Sound Play Once")  },
            { SOUND_PLAY_LOOP      , new DLFileCommand(SOUND_PLAY_LOOP  , "SOUND_PLAY_LOOP","Sound Play Loop")  },
            { SHIFT_X_PLUS         , new DLFileCommand(SHIFT_X_PLUS     , "SHIFT_X_PLUS",   "X+")  },
            { SHIFT_X_MINUS        , new DLFileCommand(SHIFT_X_MINUS    , "SHIFT_X_MINUS",  "X-")  },
            { SHIFT_Y_PLUS         , new DLFileCommand(SHIFT_Y_PLUS     , "SHIFT_Y_PLUS",   "Y+")  },
            { SHIFT_Y_MINUS        , new DLFileCommand(SHIFT_Y_MINUS    , "SHIFT_Y_MINUS",  "Y-")  },
            { KEYBOARD_ON          , new DLFileCommand(KEYBOARD_ON      , "KEYBOARD_ON",    "K+")  },
            { KEYBOARD_OFF         , new DLFileCommand(KEYBOARD_OFF     , "KEYBOARD_OFF",   "K-")  },
            { DL_FILE_NAME         , new DLFileCommand(DL_FILE_NAME     , "DL_FILE_NAME",   "#<nome_dl>")  },
            { SET_X_Y              , new DLFileCommand(SET_X_Y          , "SET_X_Y",        "(X,Y)")  },
            { SET_XT_YT            , new DLFileCommand(SET_XT_YT        , "SET_XT_YT",      "<X,Y>")  },
            { PRINT_STRING         , new DLFileCommand(PRINT_STRING     , "PRINT_STRING",   "\"<stringa>")  },
            { WAIT_KEY             , new DLFileCommand(WAIT_KEY         , "WAIT_KEY",       "Getch")  },
            { SET_COORDS_COLOR     , new DLFileCommand(SET_COORDS_COLOR , "SET_COORDS_COLOR", "VB(...)")  },
            { VB_MINUS             , new DLFileCommand(VB_MINUS         , "VB_MINUS",       "VB-")  },
            { VBR_PLUS             , new DLFileCommand(VBR_PLUS         , "VBR_PLUS",       "VBR+")  },
            { VBR_MINUS            , new DLFileCommand(VBR_MINUS        , "VBR_MINUS",      "VBR-")  },
            { MODE_F               , new DLFileCommand(MODE_F           , "MODE_F",         "F")  },
            { MODE_A               , new DLFileCommand(MODE_A           , "MODE_A",         "A")  },
            { MODE_R               , new DLFileCommand(MODE_R           , "MODE_R",         "R")  },
            { MODE_O               , new DLFileCommand(MODE_O           , "MODE_O",         "O")  },
            { PASSWORD             , new DLFileCommand(PASSWORD         , "PASSWORD",       "Password")  },
            { KEY_PROTECTED        , new DLFileCommand(KEY_PROTECTED    , "KEY_PROTECTED",  "@<chiave>,key")  },
            { SHIFT_X              , new DLFileCommand(SHIFT_X          , "SHIFT_X",        "X")  },
            { SHIFT_Y              , new DLFileCommand(SHIFT_Y          , "SHIFT_Y",        "Y")  },
        };

        public static DLFileCommand GetCommand(uint value)
        {
            DLFileCommand result = null;

            if (g_cmdTypes.TryGetValue(value, out result))
                return result;

            return null;
        }
    }
}
