﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DLViewer
{
    /// <summary>
    /// DLCommandType - Types of commands found in a DL File "frame"
    /// </summary>

    public enum DLCommandType
    {
        SHOW_IMAGE = 0,     // Display an image
        BEGIN_LOOP = 1,     // Start of a series commands with a repeat count
        END_LOOP = 2,       // End of a series of commands - go back to start
        WAIT_KEY = 3,       // Wait for user to press a key
        CLEAR_SCREEN = 4,   // Clear the screen
        UNKNOWN = 5         // Unrecognized command type
    }
}
